library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity rom_128x16 is
  port ( addr: in STD_LOGIC_VECTOR (6 downto 0);
    output: out STD_LOGIC_VECTOR (15 downto 0));
  end rom_128x16;


architecture rom_beh of rom_128x16 is
  
  type memoria is array (127 downto 0) of STD_LOGIC_VECTOR (15 downto 0);
  signal rom: memoria;
  
  begin
     -- los dos primeros d�gitos corresponden al c�digo de instrucci�n. El tercero corresponde al rs y el cuarto, al rd.
     -- se invirtieron las posiciones del rs y el rd respecto al enunciado para facilitar el dise�o.
     
    rom (0) <= x"0103";
    rom (1) <= x"0430";
    rom (2) <= x"1034";
    rom (3) <= x"1145";
    rom (4) <= x"1346";
    rom (5) <= x"1207";
    rom (6) <= x"03E4";
    rom (7) <= x"0230";
    rom (8) <= x"0240";
    rom (9) <= x"0250";
    rom (10) <= x"0260";
    rom (11) <= x"0270";
    rom (12) <= x"0280";
    rom (13) <= x"02D0";
    rom (14) <= x"02E0";
    
    output <= rom (conv_integer(addr));
    
end rom_beh;    

    



