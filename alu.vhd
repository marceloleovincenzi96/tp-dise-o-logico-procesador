library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;

entity ALU is
  port (a: in std_logic_vector (7 downto 0);
    b: in std_logic_vector (7 downto 0);
    op: in std_logic_vector (2 downto 0);
    s: out std_logic_vector (7 downto 0)
  );
    
  end ALU;
  
architecture ALU_arch of ALU is 
begin
  process (op, a, b)
    begin
      case (op) is
        when "000" => s <= a; -- salida es entrada a
        when "001" => s <= a (6 downto 0) & '1'; -- corrimiento a la izquierda
        when "010" => s <= a+b; -- salida es suma de entradas
        when "011" => s <= a-b; -- salida es resta de entradas
        when "100" => s <= a and b; -- salida es AND entre entradas
        when "101" => s <= a or b; -- salida es OR entre entradas
        when "110" => s <= a xor b; -- salida es XOR entre entradas
        when "111" => s <= '1' & a (7 downto 1); -- corrimiento a la derecha
        when others => s <= b; -- salida es entrada b
        
      end case;
      
    end process;
    
  end ALU_arch;
   
  
