library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity program_counter is

port (clk : in std_logic;
      rst : in std_logic;
      output: out std_logic_vector (6 downto 0)
      );
      
end program_counter;

architecture beh_pc of program_counter is

  signal contador: std_logic_vector (6 downto 0);
  begin
    process (clk, rst)
      begin
        if (rst = '1') then
          contador <= (others => '0');
        elsif (rising_edge(clk)) then
          contador <= contador + 1;
      end if;
    end process;
    
    output <= contador;
  end beh_pc;
      