library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity decode is
        port( input : in STD_LOGIC_VECTOR (15 downto 8);
              out_we : out STD_LOGIC;
              reg_we : out STD_LOGIC;
              ALU_op : out STD_LOGIC_VECTOR (2 downto 0);
              reg_a_we : out STD_LOGIC;
              bus_sel : out STD_LOGIC_VECTOR (1 downto 0));
              
         end decode;
         
 architecture decode_arq of decode is
 
 begin
 
 -- dise�o basado en la tabla hecha para el punto 1 del tp
 
 out_we <= '1' when (input = x"02") else '0';
 reg_we <= '0' when (input = x"04") or (input = x"05") or (input = x"02") else '1';
 ALU_op <= "010" when (input = x"10") else 
           "011" when (input = x"11") else 
           "100" when (input = x"12") else
           "101" when (input = x"13") else 
           "110" when (input = x"14") else  
           "001" when (input = x"20") else 
           "111" when (input = x"21") else  "000" ;
 reg_a_we <= '1' when (input = x"04") or (input = x"05") else '0';
 bus_sel <= "10" when (input = x"01") else
            "01" when (input = x"05") else "00";
 
end decode_arq;           