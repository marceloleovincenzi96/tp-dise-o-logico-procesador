library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;

entity mux3_1 is

port ( a: in std_logic_vector(7 downto 0);
  b: in std_logic_vector(7 downto 0);
  c: in std_logic_vector(7 downto 0);
  sel: in std_logic_vector(1 downto 0);
  s: out std_logic_vector (7 downto 0)
  );
  
end mux3_1;

architecture comportamiento of mux3_1 is

begin
  
  process (sel, a, b, c) is
    
    begin
      
      case (sel) is
        
        when "00" => s <= a;
        when "01" => s <= b;
        when "10" => s <= c;
        when others =>  s <= (others => '0');
      end case;
  end process;

end comportamiento;
        
