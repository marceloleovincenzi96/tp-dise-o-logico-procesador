library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity registro_instrucciones is
  
  port ( input: in STD_LOGIC_VECTOR (15 downto 0);
    we: in STD_LOGIC;
    clk: in STD_LOGIC;
    rst: in STD_LOGIC;
    output: out STD_LOGIC_VECTOR (15 downto 0));
    
  end registro_instrucciones;
  
architecture beh_registro_ins of registro_instrucciones is

  signal dato: STD_LOGIC_VECTOR (15 downto 0);
  
  begin
  process (clk, rst)
    begin
      
      if (rst = '1') then
        
        dato <= (others => '0');
        
      elsif (rising_edge(clk)) then
        
        if (we <= '1') then
          
          dato <= input;
          
        end if;
      end if;
      
    end process;
    
    output <= dato;
    
  end beh_registro_ins;
        
    
    
    
