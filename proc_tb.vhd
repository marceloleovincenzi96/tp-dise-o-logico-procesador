-- testbench del procesador 
-- Alumnos: Vincenzi - Frasson

Library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.Std_logic_unsigned.ALL;


entity Proc_tb is
end Proc_tb;


architecture  Proc_tb_arq of Proc_tb is 

  -- declaramos las entradas del procesador 
  
  component Proc
    
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           input : in  STD_LOGIC_VECTOR (7 downto 0);
           output : out  STD_LOGIC_VECTOR (7 downto 0)
           );
           
           
 end component;
 
 -- declaramos las se�ales del clock, reset, entrada y salida de estimulos
 
 signal clk : STD_LOGIC;
 signal rst : STD_LOGIC;
 signal in_proc : STD_LOGIC_VECTOR(7 DOWNTO 0);
 signal out_proc : STD_LOGIC_VECTOR(7 DOWNTO 0);
 
  
 -- clock del procesador = 10 ns
 constant clock : time:= 10 ns;
 
 
 
 begin
 
 
    uut : Proc port map (clk => clk ,
                         rst => rst , 
                         input => in_proc ,
                         output => out_proc
                         );
      --proceso del clock 
      Pclk:  process
        begin
          clk <= '1';
          wait for clock;
          clk<= '0';
          wait for clock;  
      end process;
      
      
      -- al proceso de reset se le da un periodo multiplicado por 30 para dar un tiempo dem�s y que se ejecuten todas las instrucciones almacenadas 
     PReset :process
       begin
       rst <= '1';
       wait for clock/2;
       rst <= '0';
       wait for clock*30;
     end process;
      
      --el est�mulo de entrada, cuyo valor es arbitrario, por eso se le asigna un 2.
      IN_preset: process
        begin
        wait for clock;
        in_proc <=x"02";
        wait;
      end process;
      
    
end Proc_tb_arq;