library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Reg_8 is
   Port ( input : in  STD_LOGIC_VECTOR (7 downto 0);
          we : in  STD_LOGIC;
          output : out  STD_LOGIC_VECTOR (7 downto 0);
          rst : in STD_LOGIC;
          clk : in STD_LOGIC
          );
    
end Reg_8;
  
architecture Reg_8_arq of Reg_8 is
  
signal dato : STD_LOGIC_VECTOR  (7 downto 0);
  
begin
  process (clk,rst)
    begin
    if (rst = '1') then
      dato <= (others => '0');
    elsif(rising_edge(clk)) then
        if (we = '1') then
           dato <= input;
        end if;
    end if;
  end process;
  output <= dato;
end Reg_8_arq;
    
    
  
  
      
