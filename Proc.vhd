----------------------------------------------------------------------------------
-- Realizado por la catedra  Dise�o L�gico (UNTREF) en 2015
-- Tiene como objeto brindarle a los alumnos un template del procesador requerido
-- Profesores Mart�n V�zquez - Lucas Leiva
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Proc is
    Port ( clk : in  std_logic;
           rst : in  std_logic;
           input : in  STD_LOGIC_VECTOR (7 downto 0);
           output : out  std_logic_vector (7 downto 0)
           );
end Proc;

architecture Beh_Proc of Proc is

-- ================
-- Declaraci�n de los componentes utilizados



component Regs 
    Port ( clk : in  std_logic;
           rst : in  std_logic;
           we : in  std_logic;
           rd : in  std_logic_vector (3 downto 0);
           rs : in  std_logic_vector (3 downto 0);
           din : in  std_logic_vector (7 downto 0);
           dout : out  std_logic_vector (7 downto 0));
end component;

component ALU port (op: in  std_logic_vector(2 downto 0);
                    A,B : in  std_logic_vector (7 downto 0);
                    S : out  std_logic_vector (7 downto 0));
end component;

component rom_128x16 port (addr : in  std_logic_vector (6 downto 0);
					          output : out  std_logic_vector (15 downto 0));
end component; 


component decode port (input : in  std_logic_vector (7 downto 0);
					              reg_we : out  std_logic;
					              out_we : out  std_logic;
					              reg_a_we: out  std_logic;
					              alu_op : out  std_logic_vector (2 downto 0);
					              bus_sel : out  std_logic_vector (1 downto 0));
end component; 

component Reg_8 port(input : in  STD_LOGIC_VECTOR (7 downto 0);
                     we : in  STD_LOGIC;
                     output : out  STD_LOGIC_VECTOR (7 downto 0);
                     rst : in STD_LOGIC;
                     clk : in STD_LOGIC);
end component;

component registro_instrucciones port(input : in  STD_LOGIC_VECTOR (15 downto 0);
                      output : out  STD_LOGIC_VECTOR (15 downto 0);
                      rst : in STD_LOGIC;
                      we : in  STD_LOGIC;
                      clk : in STD_LOGIC);
end component;

component program_counter port(clk : in STD_LOGIC;
                                 rst : in STD_LOGIC;
                                 output : out STD_LOGIC_VECTOR (6 downto 0));
end component;

component mux3_1 port ( a: in std_logic_vector(7 downto 0);
  b: in std_logic_vector(7 downto 0);
  c: in std_logic_vector(7 downto 0);
  sel: in std_logic_vector(1 downto 0);
  s: out std_logic_vector (7 downto 0)
  );
end component;


-- ================

-- ================
-- declaraci�n de se�ales usadas 

-- Banco de registros
 
signal out_REGS : STD_LOGIC_VECTOR(7 downto 0);


-- Contador de programa
signal out_pc : STD_LOGIC_VECTOR(6 DOWNTO 0);

--ROM 
signal out_rom : STD_LOGIC_VECTOR(15 DOWNTO 0);

-- Registro de instrucciones

signal out_reg_ins : STD_LOGIC_VECTOR(15 DOWNTO 0);


-- decode
signal out_we_bus: STD_LOGIC;
signal reg_we_bus: STD_LOGIC; 
signal reg_a_we_bus: STD_LOGIC;
signal alu_op_bus: STD_LOGIC_VECTOR(2 downto 0);
signal bus_sel_bus: STD_LOGIC_VECTOR(1 downto 0);

-- ALU
signal S_ALU: STD_LOGIC_VECTOR(7 downto 0);

-- MUX 
signal out_MUX: STD_LOGIC_VECTOR(7 downto 0);


-- Reg_A 

signal out_reg_A: STD_LOGIC_VECTOR(7 downto 0);





-- ================
begin

-- ================
-- Instaciacion de componentes utilziados

-- Banco de registros
banco_regs:  Regs Port map (clk => clk,
                       rst => rst,
                       we => reg_we_bus, 
								       rd => out_reg_ins(3 downto 0),
								       rs => out_reg_ins(7 downto 4), 
								       din =>S_ALU,
								       dout =>out_REGS ); 
-- ALU
la_ALU: ALU port map (op => alu_op_bus,
                    A => out_MUX,
                    B => out_reg_a,               
                    S => S_ALU);
  
-- Contador de programa
Pc: program_counter port map(clk => clk,
                              rst => rst,
                              output => out_pc);
  
  
-- ROM de programa
ROM_Prog: rom_128x16 port map (addr => out_pc,
                              output => out_rom);

-- decode
decoder: decode port map (input => out_reg_ins(15 downto 8),
					                reg_we => reg_we_bus,
					                out_we => out_we_bus,
					                reg_a_we => reg_a_we_bus,
					                alu_op => alu_op_bus,
					                bus_sel => bus_sel_bus);
  
-- Registro de instrucciones

REG_Ins: registro_instrucciones port map(clk => clk,
                         rst => rst,
                         input => out_rom,
                         output => out_reg_ins,
                         we => clk);
                         

-- REG_A
REG_A : Reg_8 port map(we => reg_a_we_bus,
                        clk => clk,
                        rst => rst,
                        input => out_MUX,
                        output =>out_reg_A);
                        
-- REG_OUT

REG_OUT : Reg_8 port map(we => out_we_bus,
                          clk => clk,
                          rst => rst,
                          input => S_ALU,
                          output => output);
-- Mux                          
MUX: mux3_1 port map (a => out_REGS,
                      b => out_reg_ins (7 downto 0),
                      c => input,
                      sel => bus_sel_bus,
                      s => out_MUX);


end Beh_Proc;

